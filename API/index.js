const express = require("express");
const mongoose = require("mongoose");

const app = express();

app.use(express.json());

const db = mongoose.connection;

app.get("/", async (request, response) => {
    mongoose.connect('mongodb://root:example@mongo:27017/',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
    );
    db.on("error", console.error.bind(console, "connection error: "));
    db.once("open", function () {
        console.log("Connected successfully");
        response.send("Edgar Cil 201503600 - Conexion exitosa!")
    });
  });

app.listen(3000, () => {
  console.log("Server is running at port 3000");
});